import scala.annotation.tailrec

/**
 * @author Slava Stashuk
 */
object MSort extends App {

  case class Inversions(list: List[Int], inversions: Long) {
    def :::(frontList: List[Int]) = Inversions(frontList ::: list, inversions)
    def ::(frontElem: Int) = Inversions(frontElem :: list, inversions)
    def reverse = Inversions(list.reverse, inversions)
    def +(inv: Long) = Inversions(list, inversions + inv)
  }

  object Inversions {
    def apply(): Inversions = this(Nil, 0)
    def apply(list: List[Int]): Inversions = this(list, 0)
  }

  /**
   * we don't care about @tailrec because the depth of recursion is log n which is small
   * <p>the helper method with size param is a little optimisation since size operation on the list is expensive (O(n))
   * @param list
   * @return
   */
  def sort(list: List[Int]): Inversions = {
    def sort(list: List[Int], size: Int): Inversions =
      list match {
        case Nil => Inversions(list)
        case _ :: Nil => Inversions(list)
        case _ =>
          val sizeOver2 = size / 2
          val parts = list.splitAt(sizeOver2)

          val sorted1 = sort(parts._1, sizeOver2)
          val sorted2 = sort(parts._2, size - sizeOver2)
          val m = merge(sorted1.list, sorted2.list, Inversions())

          m.reverse + sorted1.inversions + sorted2.inversions
      }

    sort(list, list.size)
  }

  //2407905288

  /**
   * here having @tailrec is crucial, the depth of recursion is n i.e. can easily overflow
   * @param first
   * @param second
   * @param inversions
   * @return
   */
  @tailrec
  private def merge(first: List[Int], second: List[Int], inversions: Inversions): Inversions = {
    (first, second) match {
      case (first, Nil) => first.reverse ::: inversions
      case (Nil, second) => second.reverse ::: inversions
      case (firstHead :: firstTail, secondHead :: secondTail) =>
        if (firstHead <= secondHead) {
          merge(firstTail, second, firstHead :: inversions)
        } else {
          merge(first, secondTail, secondHead :: inversions + first.size)
        }
    }
  }

  import scala.io.Source

  val input = Source.fromInputStream(getClass.getResourceAsStream("input")).getLines().map(_.toInt)

  val output = sort(input.toList)

  println(output.inversions)

}
