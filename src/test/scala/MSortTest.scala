
import org.scalatest._

class MSortTest extends FlatSpec with Matchers {

  "MSort" should "sort an empty array" in {
    val list = Nil
    val sorted = MSort.sort(list)
    sorted.list should be(Nil)
    sorted.inversions should be(0L)
  }

  "MSort" should "sort a 1 elem array" in {
    val list = List(1)
    val sorted = MSort.sort(list)
    sorted.list should be(List(1))
    sorted.inversions should be(0L)
  }

  "MSort" should "sort an arbitrary array" in {
    val list = List(2, 6, 9, 1, 6, 7, 7)
    val sorted = MSort.sort(list)
    sorted.list should be(List(1, 2, 6, 6, 7, 7, 9))
    sorted.inversions should be(6L)
  }

  "MSort" should "sort an arbitrary array 2" in {
    val list = List(2, 6, 9, 1, 6, 7, 7, 4, 8, 4, 8, 7, 6, 0)
    MSort.sort(list).list should be(list.sorted)
    println(MSort.sort(list).inversions)
  }

}


